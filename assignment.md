# Week 11 - GRPC

Vaším úkolem bude implementovat gRPC server a klienta.

## Architektura

![](./diagrams/week-11.png)

## Spuštění Game Service

1. `docker compose up`: spustí databázi
2. `sqlx database create`: vytvoří databázi
3. `sqlx migrate run`: spustí migraci, vytvoří tabulky a testovací data
4. `cargo run`: spuštění appky

## Spuštění Game Web

1. `cargo run`: spuštění appky

## Todo

-   Vytvořit v proto souboru nadefinování pro service, která vrátí všechny uživatele
-   Implementovat Game Service, který vezme uživatele z databáze a pošle je přes gRPC na client
-   Implementovat volání Game Service z Game Web
-   Přidat vygenerované typy do Askama Templates, aby jste v prohlíéžeči vypsali uživatele

## Tips

-   Postman nebo Insomnia umí provolávat gRPC. To je skvělé na testování Vaší implementace Game Service
-   Některé složitejší věci jsou jen zakomentované, takže se nebojte inspirovat

## IDE podpora generovaných typů

### rust-analyzer

`"rust-analyzer.cargo.buildScripts.enable": true`

### IntelliJ Rust

Experimental Features -> `org.rust.cargo.evaluate.build.scripts`
