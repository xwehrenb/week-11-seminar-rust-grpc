use crate::models::Score;
use async_trait::async_trait;
use sqlx::PgPool;
use std::sync::Arc;

#[async_trait]
pub trait ScoreRepo {
    async fn add_score(&self, player_id: i32, score: i32) -> anyhow::Result<i32>;
    async fn list_scores(&self) -> anyhow::Result<Vec<Score>>;
    async fn list_top_scores(&self) -> anyhow::Result<Vec<Score>>;
}

#[derive(Debug)]
pub struct PostgresScoreRepo {
    pg_pool: Arc<PgPool>,
}

impl PostgresScoreRepo {
    pub fn new(pg_pool: Arc<PgPool>) -> Self {
        Self { pg_pool }
    }
}

#[async_trait]
impl ScoreRepo for PostgresScoreRepo {
    async fn add_score(&self, player_id: i32, score: i32) -> anyhow::Result<i32> {
        let row = sqlx::query!(
            "INSERT INTO score (player_id, score)
             VALUES ($1, $2) RETURNING id",
            player_id,
            score,
        )
        .fetch_one(&*self.pg_pool)
        .await?;
        Ok(row.id)
    }

    async fn list_scores(&self) -> anyhow::Result<Vec<Score>> {
        let scores = sqlx::query_as!(Score, "SELECT * FROM score")
            .fetch_all(&*self.pg_pool)
            .await?;

        Ok(scores)
    }

    async fn list_top_scores(&self) -> anyhow::Result<Vec<Score>> {
        let scores = sqlx::query_as!(Score, "SELECT * FROM score ORDER BY score DESC LIMIT 10")
        .fetch_all(&*self.pg_pool)
        .await?;

        Ok(scores)
    }
}
