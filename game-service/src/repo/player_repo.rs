use crate::models::Player;
use async_trait::async_trait;
use sqlx::PgPool;
use std::sync::Arc;

#[async_trait]
pub trait PlayerRepo {
    async fn add_player(&self, name: String, email: String) -> anyhow::Result<i32>;
    async fn list_players(&self) -> anyhow::Result<Vec<Player>>;
}

#[derive(Debug)]
pub struct PostgresPlayerRepo {
    pg_pool: Arc<PgPool>,
}

impl PostgresPlayerRepo {
    pub fn new(pg_pool: Arc<PgPool>) -> Self {
        Self { pg_pool }
    }
}

#[async_trait]
impl PlayerRepo for PostgresPlayerRepo {
    async fn add_player(&self, name: String, email: String) -> anyhow::Result<i32> {
        let row = sqlx::query!(
            "INSERT INTO player (username, email)
             VALUES ($1, $2)
             RETURNING id",
            name,
            email
        )
        .fetch_one(&*self.pg_pool)
        .await?;

        Ok(row.id)
    }

    async fn list_players(&self) -> anyhow::Result<Vec<Player>> {
        let players = sqlx::query_as!(Player, "SELECT * FROM player")
            .fetch_all(&*self.pg_pool)
            .await?;

        Ok(players)
    }
}
