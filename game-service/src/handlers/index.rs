use crate::repo::player_repo::PostgresPlayerRepo;
use crate::repo::score_repo::PostgresScoreRepo;
use std::sync::Arc;

#[derive(Debug)]
pub struct GameServiceHandler {
    pub player_repo: Arc<PostgresPlayerRepo>,
    pub score_repo: Arc<PostgresScoreRepo>,
}

//#[tonic::async_trait]
//impl Game for GameServiceHandler {}
