use std::env;
use std::sync::Arc;

use dotenv::dotenv;
use sqlx::postgres::PgPoolOptions;
use sqlx::{Pool, Postgres};

use crate::repo::player_repo::PostgresPlayerRepo;
use crate::repo::score_repo::PostgresScoreRepo;

mod models;
mod repo;
mod handlers;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let pool = Arc::new(setup_db_pool().await?);

    let _score_repo = PostgresScoreRepo::new(pool.clone());
    let _player_repo = PostgresPlayerRepo::new(pool.clone());

    let _addr = "127.0.0.1:8001";

    Ok(())
}

async fn setup_db_pool() -> anyhow::Result<Pool<Postgres>> {
    dotenv()?;
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await?;
    Ok(pool)
}
