CREATE TABLE player (
    id          SERIAL          PRIMARY KEY,
    username    TEXT            NOT NULL,
    email       TEXT            NOT NULL
);

CREATE TABLE score (
    id              SERIAL      PRIMARY KEY,
    player_id       INTEGER     NOT NULL,
    score           INTEGER     NOT NULL,
    
    CONSTRAINT fk_player FOREIGN KEY(player_id) REFERENCES player(id)
);
