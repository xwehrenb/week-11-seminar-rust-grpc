use askama::Template;

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexPageTemplate {}

/*
#[derive(Template)]
#[template(path = "players.html")]
pub struct PlayersPageTemplate {
    pub players: Vec<Player>,
}

#[derive(Template)]
#[template(path = "scores.html")]
pub struct ScoresPageTemplate {
    pub scores: Vec<Score>,
}

#[derive(Template)]
#[template(path = "leaderboard.html")]
pub struct LeaderboardPageTemplate {
    pub scores: Vec<Score>,
}*/

#[derive(Template)]
#[template(path = "not-implemented.html")]
pub struct NotImplementedPageTemplate {}
