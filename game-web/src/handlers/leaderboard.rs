use actix_web::{get, HttpResponse, Responder};

use crate::templates::NotImplementedPageTemplate;
use askama::Template; 

#[get("/leaderboard")]
pub async fn get() -> impl Responder {
    let not_implemented_template = NotImplementedPageTemplate {};
    
    let Ok(body) = not_implemented_template.render() else {
        return HttpResponse::InternalServerError().body("ouch.... our fail");
    };

    HttpResponse::Ok().content_type("text/html").body(body)
}
