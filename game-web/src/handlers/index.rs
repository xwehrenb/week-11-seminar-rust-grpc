use actix_web::{get, HttpResponse, Responder};
use crate::templates::IndexPageTemplate;
use askama::Template;

#[get("/")]
pub async fn get() -> impl Responder {
    let index_template = IndexPageTemplate {};
    
    let Ok(body) = index_template.render() else {
        return HttpResponse::InternalServerError().body("ouch.... our fail");
    };

    HttpResponse::Ok().content_type("text/html").body(body)
}
