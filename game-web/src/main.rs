use actix_web::{App, HttpServer};
use handlers::{players, leaderboard, index, scores};

mod handlers;
mod templates;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // let mut game_client = /* yout client */ 
    
    HttpServer::new(move || {
        App::new()
            //.app_data(Data::new(Mutex::new(game_client.clone())))
            .service(players::get)
            .service(leaderboard::get)
            .service(scores::get)
            .service(index::get)
    })
    .bind(("127.0.0.1", 8000))?
    .run()
    .await
}
